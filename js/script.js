// total pokemon
pokemons = [];
maxItems = 248;
min = 1;

function RandomId() {
    return Math.floor(Math.random() * 248) + 1;
}


function makeCard(id, dato){
    var div = document.getElementById(id);
    // get header div children from div make img and insert it into
    var container = div.children[0];
    var img = document.createElement("img");
    img.src = dato.sprites.front_default;
    container.appendChild(img);
}


const getPokemon = (i) => {
    url = 'https://pokeapi.co/api/v2/pokemon/' + RandomId() + '/'
    axios.get(url)
        .then(
            response => {
                const dato = response.data;
                console.log(i)
                if(i==0){
                    makeCard("frs", dato);
                }
                if(i==1){
                    makeCard("snd", dato);
                }
                if(i==2){
                    makeCard("trt", dato);
                }
                if(i==3){
                    makeCard("fr", dato);
                }
                if(i==4){
                    makeCard("fv", dato);
                }
                if(i==5){
                    makeCard("sx", dato);
                }
               
            })
        .catch(error => console.error(error));
};

async function getPokemons() {
    for (i = 0; i < 6; i++) {
     await getPokemon(i);
    }
}

window.onload = function () {
    getPokemons();
};
